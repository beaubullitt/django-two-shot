from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.conf import settings
from receipts.forms import CreateReceipt, CreateCategoryForm, CreateAccountForm


# Create your views here.
@login_required
# IF Receipt.user or user.Receipts ==
def Receipt_mold(request):
    receipt_data = Receipt.objects.filter(purchaser=request.user)
    # .filter(purchaser=request.user)
    context = {
        "receipt_data": receipt_data,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    # if they're submitting a post for their form
    if request.method == "POST":
        print("USER IS: ", request.user)
        # mold the sumbitted date to the form
        new_receipt = CreateReceipt(request.POST)
        # new_receipt.purchaser = request.user
        # and the form is valid
        print("MOOOOOLLLDEEEEEEDDDD")
        if new_receipt.is_valid():
            print("IIITSSSSSSS VAAAAALLLIIDDD")
            # saves momentarily
            polished_receipt = new_receipt.save(False)
            # add purchaser
            polished_receipt.purchaser = request.user
            # Saves to Database
            polished_receipt.save()
            return redirect("home")
    else:
        new_receipt = CreateReceipt()

    context = {
        "form": new_receipt,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    expense_data = Receipt.objects.filter(purchaser=request.user)
    print("EXXXXPENSE DATA IS: ", expense_data)
    context = {
        "expense_data": expense_data,
    }
    return render(request, "receipts/category.html", context)


@login_required
def account_list(request):
    account_data = Receipt.objects.filter(purchaser=request.user)
    print("ACCOUNT DATA IS: ", account_data)
    context = {
        "account_data": account_data,
    }
    return render(request, "receipts/account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            form.owner = request.user
            return redirect(category_list)
    else:
        form = CreateCategoryForm()
        context = {
            "form": form,
        }
        return render(request, "receipts/category_create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            form.save()
            form.owner = request.user
            return redirect(account_list)
    else:
        form = CreateAccountForm()
        context = {
            "form": form,
        }
        return render(request, "receipts/account_create.html", context)

from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth import login, authenticate, logout
from receipts.views import Receipt_mold
from django.contrib.auth.models import User


# Create your views here.
def user_login(request):
    # if user is submitting log in data, POST
    if request.method == "POST":
        # variable equals the request API in the shape of my form
        form = LoginForm(request.POST)
        # if request API/form mold is valid
        if form.is_valid():
            # username and pw variables assigned to my form's
            # cleaned dictionary values
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # built in authentification
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            # if user exists i guess
            # ??? why not none? is this a django thing?
            if user is not None:
                # built in login
                login(request, user)
                return redirect(Receipt_mold)

    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect(user_login)


def signup(request):
    if request.method == "POST":
        signup_data = SignUpForm(request.POST)
        if signup_data.is_valid():
            username = signup_data.cleaned_data["username"]
            password = signup_data.cleaned_data["password"]
            password_confirmation = signup_data.cleaned_data[
                "password_confirmation"
            ]

            if password == password_confirmation:
                user = User.objects.create_user(username, password)
                user.save()
                login(request, user)
                return redirect(Receipt_mold)
            elif password != password_confirmation:
                signup_data.add_error("password", "Passwords do not match")
    else:
        signup_data = SignUpForm(request.GET)
    context = {
        "form": signup_data,
    }
    return render(request, "accounts/signup.html", context)

from django.forms import ModelForm
from receipts.models import Receipt, Account, ExpenseCategory


class CreateReceipt(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class CreateCategoryForm(ModelForm):
    class Meta:
        moodel = ExpenseCategory
        fields = [
            "name",
        ]


class CreateAccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
